# `clikan.sh` command line kanban

This shell script provides an interactive command line
interface for manipulating files with Markdown formatted lists.

Each list item corresponds to a card of a simple kanban system.
The three possible list bullets `-*+` are used as indicators of
status "wait/backlog", "doing", and "done/archived".

The script permits displaying and status changes of the items,
addition of new "waiting" items, direct edition of the
lists ("kanban" files) and of the configuration file defining
kanbans and some script parameters.

In addition, it will display as "doing" each list item of the
pattern `- ...(MM-DD)...` where `MM-DD` corresponds to the
current month and day,
and it can also filter for cards containing a filter keyword.

## Installation

Only the script `clikanban.sh` must be installed somewhere,
where it can be executed either directly, or as argument to
`sh` or another reasonable shell. Currently no arguments are
processed.

The script currently requires the standard (POSIX conformal)
tools `sed grep ed cat mktemp cp date` for correct function;
they should be available on any sane UNIX-like system.

## Execution

The script will check for a configuration file and if necessary
create one (see Configuration further below).
It will scan the defined "kanban files" for "doing" items
(with preceding '* ') and display them, then wait for input
after a prompt.

A help screen is displayed if an empty command is given, and
then also the configuration can be displayed by entering `c`.

### Commands

- `q` : quit the program (also CTRL-D should work)
- `d` : display all "doing" entries as selection list
- `a` : display all "archived/done" entries as selection list
- `w` : display all "waiting/backlog" entries as selection list
- `do` N : change card to "doing/active" status
- `done` N : change card to "archive/done" status
   and append a timestamp of format "// YY-MM-DD"
- `wait` N : change card to "waiting/backlog" status
- `n` : add a new item/card to the target kanban (only one line of text);
   content may be given on the same line, separated by whitespace
- `k` : define target kanban file (for direct edition, and new items)
- `f` F : define filter word F as grep pattern for selection of cards
   (empty is changed into `.` which matches all non-empty cards)
- `ek` : directly edit the target kanban file with the text editor
- `ec` : directly edit the configuration file with the text editor
- `!` : execute arguments with shell, `/bin/sh -c` by default
   (set `myshell` variable in source to `echo` to deactivate this)

In the list above, "N" is a number from the last displayed selection list;
if none is given, the script asks for one. The numbers are not attributed
to specific "cards" but to the items in the last displayed selection list
(i.e the first "doing" and the first "waiting" card both have number 1,
and it depends on the selection context, which one is referred to).
The commands `do/done/wait` also accept several number arguments
separated by white space.

"F" is a word used for filtering for specific cards. If the filter word is
non-empty, only cards containing this word are displayed. Can be used e.g
to only select cards containing a given "topic". Please note: this filter
word actually is used as a grep string; dots will match any character and
therefore should not be used in "topics" (leading `+` signs and `-_:,`
anywhere are fine, though).

Be careful with the `ek` and the `ec` commands, as the syntax of the
corresponding files must be preserved for the script to correctly work.
Whitespace in general is no problem, though.

Cards/items do not have any kind of ID number; they are just text, and
therefore some strange things may happen if the same entry exists in
more than one "kanban file" -- in particular, they will all change
status (doing/waiting/done) together.
No card may contain TAB characters; they are replaced by SPC at entry.
Do not add TAB charactes manually through the `ek` command, as this
will probably disturb the script's manipulation of such cards.

The `d` command will not only display items with a preceding `*`
but also those with a pattern of `(MM-DD)` anywhere after preceding
`-*+` characters, with `MM` being the current month and `DD` the
current day. This permits to use a "kanban file" as a calendar; in
that case it should be given as last `kanban` entry, to prevent
filling of the screen with calendar entries when using any of the
`d w a` commands (see also example below).
In addition, the `d` command will sort the output cards according
to the configuration variable `sortdir` (see below).

The `!` "shell escape" command will allow you to execute shell commands.
If the command begins with a letter and does not match to an internal
clikan command, you may even omit the leading `!` -- so be careful:
only type `/bin/rm -rf *` at the clikan prompt if you *absolutely*
know what you're doing! You've been warned!
(Also note commands are of course executed in a subshell,
therefore typing `cd ..` is rather pointless.)

## Configuration

The script expects a configuration file named by the environment
variable `$CLIKANCONF` or (if undefined) `$HOME/.clikanconf` by default.
If there isn't any, the script generates a new one, populated
with all possible parameters at reasonable default values, and
also creates an item list ("kanban file") with the name
`$HOME/clikanban.md` containing some dummy entries.

Please see the source or the generated configuration file for
information about possible settings!

*Note: if `$HOME/clikanban.md` already exists, the script will
save a backup copy as `$HOME/clikanban.md.bak` in case
something went wrong with the configuration file.*

In addition, it uses the contents of `$VISUAL` or `$EDITOR`
when needing to launch a text editor, or `ed` as default.
*(Because, what else!?)*

## Example

We assume a first start with no environment variables set.

    $ ./clikan.sh
     no config found, generating default config file /home/username/.clikanconf
     and default kanban file /home/username/clikanban.md
     (this should happen only once at first use)

       doing
    1: learn to use clikan

Default config and kanban files have been generated.
Let's see the waiting, the archived/done, and again the "doing" cards:

    |< w

       waiting/todo/backlog
    1: improve clikan
    |< a

       archived/done
    1: install clikan
    |< d

       doing
    1: learn to use clikan

Now let's assume we know everything, so card 1 is done:

    |< done 1

       doing
    |< w

       waiting/todo/backlog
    1: improve clikan

Obviously there is work waiting, so let's do it:

    |< do 1

       doing
    1: improve clikan

Now we check for the target kanban ...

    |< k
    1: /home/username/clikanban.md
    choice? (default: /home/username/clikanban.md)


       doing
    1: improve clikan

... do a manual editing session on it ...

    |< ek
    calling /bin/ed /home/username/clikanban.md ...
    158
    1z
    # my kanban file

    + learn to use clikan
    * improve clikan
    + install clikan

    ### titles work as comments
    # might be a future card you don't yet want in waiting

... change one entry ...

    ?improve
    * improve clikan
    s/improve/test/p
    * test clikan

... and save it, quit, and we're back at our script:

    w
    155
    q
    welcome back

Verifying the entry indeed has been changed:

    |< d

       doing
    1: test clikan

Now let's add a new card:

    |< n
    enter new card for target /home/username/clikanban.md :
    give feedback to clikan developer

       doing
    1: test clikan

The new card has been added to the waiting/backlog list:

    |< w

       waiting/todo/backlog
    1: give feedback to clikan developer

We could also have given the card content as direct argument, by typing `n give feedback...` at the prompt.

Just hitting RETURN at the prompt shows the help/usage page:

    |<
    ./clikan.sh : command line kanban
      (2020-05-11/HB9KNS)
    configuration file: /home/username/.clikanconf
    for help about commands, just hit RETURN
    to show configuration, enter 'c'

We want to see the configuration, then quit:

    c
     CLIKANCONF=
     configfile=/home/username/.clikanconf
     editor=/bin/ed
     maxshow=15
     configfile contents:
        # configuration file for ./clikan.sh
        # (autogenerated at Tue Dec 17 07:18:26 UTC 2019)
        # file name can be defined by env.var CLIKANCONF
        #
        #  prompt is displayed when waiting for command
        prompt |<
        #  maxshow defines the maximum number of cards/lines to be displayed
        maxshow 15
	#  sortdir defines order of non-calendar kanban cards
	#  and can be one of up, down or none (default)
	sortdir none
        #  kanban defines a kanban file, may be given several times
        # kanban /some/path/to/jobkanban.txt
        # kanban /another/path/to/privatekanban.md
        kanban /home/username/clikanban.md

       doing
    1: test clikan
    |< q

Please note: the `maxshow` variable is not honoured when displaying
the archived/done entries: in that case, the display limit is set
to an arbitrary value of 99999 (i.e reasonably indefinite). It is
up to the user to do some housekeeping now and then in the archive,
by manually removing the entries marked with a `+` flag through the
`ek` command.

---

*2020-05-11/HB9KNS,Y.Bonetti*
